var selektovani_objekat = null;

/* Stanje */
Raphael.fn.stanje = function stanje(id, tip, tekst, x, y, r) {
	var stanje = null;
	var objekat = r.set();
	
	if (tip == "terminalno_stanje") {
		var krug = r.circle(x, y, 30).attr({"fill": "#fff", "fill-opacity" : 1, "stroke-width": 2, "stroke-linecap": "round"});
		krug.data("roditelj", id);
		krug.click(stanje_mouseclick);
		krug.dblclick(stanje_mousedblclick);
		krug.mouseover(stanje_mouseover);
		krug.mouseout(stanje_mouseout);
		objekat.push(krug);

		krug = r.circle(x, y, 25).attr({"fill": "#fff", "fill-opacity" : 1, "stroke-width": 2, "stroke-linecap": "round"});
		krug.data("roditelj", id);
		krug.click(stanje_mouseclick);
		krug.dblclick(stanje_mousedblclick);
		krug.mouseover(stanje_mouseover);
		krug.mouseout(stanje_mouseout);
		objekat.push(krug);
	} else {
		var krug = r.circle(x, y, 30).attr({"fill": "#fff", "fill-opacity" : 1, "stroke-width": 2, "stroke-linecap": "round"});
		krug.data("roditelj", id);
		krug.click(stanje_mouseclick);
		krug.dblclick(stanje_mousedblclick);
		krug.mouseover(stanje_mouseover);
		krug.mouseout(stanje_mouseout);
		objekat.push(krug);
	}

	var tekst_stanja = r.text(x, y, tekst).attr({fill: "#000", "font-size": 14, "font-weight" : "bold"});
	tekst_stanja.data("roditelj", id);
	tekst_stanja.click(stanje_mouseclick);
	tekst_stanja.dblclick(stanje_mousedblclick);
	tekst_stanja.mouseover(stanje_mouseover);
	tekst_stanja.mouseout(stanje_mouseout);
	objekat.push(tekst_stanja);

	var update = function(dx, dy) {
		if (tip == "terminalno_stanje") {
			for (i = 0; i < objekat.length; i++) {
				if (objekat[i].type == "circle") {
					var x = (objekat[i].attr("cx")) + dx;
					var y = (objekat[i].attr("cy")) + dy;
					objekat[i].attr({"cx":x, "cy":y});
				} else {
					var x = (objekat[i].attr("x")) + dx;
					var y = (objekat[i].attr("y")) + dy;
					objekat[i].attr({"x": x, "y": y});
				}
			}
		} else {
			for (i = 0; i < objekat.length; i++) {
				if (objekat[i].type == "circle") {
					var x = (objekat[i].attr("cx")) + dx;
					var y = (objekat[i].attr("cy")) + dy;
					objekat[i].attr({"cx":x, "cy":y});
				} else {
					var x = (objekat[i].attr("x")) + dx;
					var y = (objekat[i].attr("y")) + dy;
					objekat[i].attr({"x": x, "y": y});
				}
			}
		}
	};

	stanje = {
		id: id,
		roditelj: id,
		tip: tip,
		tekst: tekst,
		objekat: objekat,
		update: update
	}

	objekat.drag(stanje_move, stanje_dragover);

	return stanje;
}

function stanje_mouseover() {
	var roditelj_id = this.data("roditelj");
	var roditelj = dajRoditeljaPoId(roditelj_id);
	eve("stanje.mouseover", "stanje", roditelj, "#0000ff");
}

function stanje_mouseout() {
	var roditelj_id = this.data("roditelj");
	var roditelj = dajRoditeljaPoId(roditelj_id);
	eve("stanje.mouseout", "stanje", roditelj);
}

function stanje_mouseclick() {
	var roditelj_id = this.data("roditelj");
	var roditelj = dajRoditeljaPoId(roditelj_id);
	eve("stanje.tofront", "stanje", roditelj);
}

function stanje_mousedblclick() {
	var roditelj_id = this.data("roditelj");
	var roditelj = dajRoditeljaPoId(roditelj_id);
	if (selektovani_objekat != roditelj) {
		if (selektovani_objekat != null) {
			eve("stanje.deselektuj", "stanje", selektovani_objekat);
		}
		eve("stanje.selektuj", "stanje", roditelj, "#ff0000");
		selektovani_objekat = roditelj;
		eve("objekat.selektovan", "objekat", selektovani_objekat);
	} else {
		selektovani_objekat = null;
		eve("stanje.deselektuj", "stanje", roditelj);
		eve("stanje.selektuj", "stanje", roditelj, "#0000ff");
		eve("objekat.selektovan", "objekat", selektovani_objekat);
	}
}

function stanje_move(dx, dy) {
	var roditelj_id = this.data("roditelj");
	var roditelj = dajRoditeljaPoId(roditelj_id);
	eve("stanje.tofront", "stanje", roditelj);
	roditelj.update(dx - (this.dx || 0), dy - (this.dy || 0));
	eve("veze.azuriraj", "veze", roditelj);
	this.dx = dx;
	this.dy = dy;
}

function stanje_dragover() {
	this.dx = 0;
	this.dy = 0;
}

/* Veza */
Raphael.fn.veza = function veza(id, pravilo, pocetno_stanje, zavrsno_stanje, r) {
	var veza = null;
	var objekat = r.set();
	var tekst = " " + pravilo.procitano + " > " + pravilo.pisi + ", " + pravilo.pomjeri + " \n";

	if (pocetno_stanje.id != zavrsno_stanje.id)	{
		obj1 = pocetno_stanje.objekat[0];
		obj2 = zavrsno_stanje.objekat[0];

		var bb1 = obj1.getBBox(),
			bb2 = obj2.getBBox(),
			p = [{x: bb1.x + bb1.width / 2, y: bb1.y - 1},
			{x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},
			{x: bb1.x - 1, y: bb1.y + bb1.height / 2},
			{x: bb1.x + bb1.width + 1, y: bb1.y + bb1.height / 2},
			{x: bb2.x + bb2.width / 2, y: bb2.y - 1},
			{x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},
			{x: bb2.x - 1, y: bb2.y + bb2.height / 2},
			{x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}],
			d = {}, dis = [];
			for (var i = 0; i < 4; i++) {
				for (var j = 4; j < 8; j++) {
					var dx = Math.abs(p[i].x - p[j].x),
						dy = Math.abs(p[i].y - p[j].y);
					if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
						dis.push(dx + dy);
						d[dis[dis.length - 1]] = [i, j];
					}
				}
			}
			if (dis.length == 0) {
				var res = [0, 4];
			} else {
				res = d[Math.min.apply(Math, dis)];
			}
			var x1 = p[res[0]].x,
				y1 = p[res[0]].y,
				x4 = p[res[1]].x,
				y4 = p[res[1]].y;
			dx = Math.max(Math.abs(x1 - x4) / 2, 10);
			dy = Math.max(Math.abs(y1 - y4) / 2, 10);
			var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
				y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
				x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
				y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);
			if (res[1] == 6) {
				// strelica udesno
				var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", Number(x2)+10, Number(y2), Number(x3)-40, Number(y3), Number(x4.toFixed(3)), Number(y4.toFixed(3))].join(",");
				objekat.push(r.path(path));
				var strelica1 = ["M", Number(x4.toFixed(0))-10,  Number(y4.toFixed(0))-7, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				objekat.push(r.path(strelica1));
				var strelica2 = ["M", Number(x4.toFixed(0))-10, Number(y4.toFixed(0))+7, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				objekat.push(r.path(strelica2));

			} else if (res[1] == 7) {
				// strelica ulijevo
				var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", Number(x2)+10, Number(y2), Number(x3)+40, Number(y3), Number(x4.toFixed(3)), Number(y4.toFixed(3))].join(",");
				objekat.push(r.path(path));
				var strelica1 = ["M", Number(x4.toFixed(0))+10,  Number(y4.toFixed(0))-7, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				objekat.push(r.path(strelica1));
				var strelica2 = ["M", Number(x4.toFixed(0))+10, Number(y4.toFixed(0))+7, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				objekat.push(r.path(strelica2));
			} else if (res[1] == 4) {
				// strelica nadole
				var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", Number(x2)+10, Number(y2), Number(x3), Number(y3)-40, Number(x4.toFixed(3)), Number(y4.toFixed(3))].join(",");
				objekat.push(r.path(path));
				var strelica1 = ["M", Number(x4.toFixed(0))-7,  Number(y4.toFixed(0))-10, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				objekat.push(r.path(strelica1));
				var strelica2 = ["M", Number(x4.toFixed(0))+7, Number(y4.toFixed(0))-10, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				objekat.push(r.path(strelica2));
			} else if (res[1] == 5) {
				// strelica nagore
				var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", Number(x2)+10, Number(y2), Number(x3), Number(y3)+40, Number(x4.toFixed(3)), Number(y4.toFixed(3))].join(",");
				objekat.push(r.path(path));
				var strelica1 = ["M", Number(x4.toFixed(0))-7,  Number(y4.toFixed(0))+10, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				objekat.push(r.path(strelica1));
				var strelica2 = ["M", Number(x4.toFixed(0))+7, Number(y4.toFixed(0))+10, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				objekat.push(r.path(strelica2));
			}

			var centar = objekat[0].getPointAtLength(objekat[0].getTotalLength(objekat[0])/2);
			objekat.push(r.text(centar.x, centar.y, tekst).attr({fill: "#fff", "font-size": 12}));
			var textBBox = objekat[3].getBBox();
			objekat.push(r.rect(textBBox.x, textBBox.y, textBBox.width, textBBox.height, 5).attr({fill: "#000"}));
			objekat[3].toFront();
	} else {
			var bbox = pocetno_stanje.objekat[0].getBBox();
			var x1 = bbox.x;
			var y1 = bbox.y;
			var width = bbox.width;
			var height = bbox.height;
			var path = ["M", x1+width/2, y1, "C", x1+width/2, y1-100, x1+width+100, y1+height/2, x1+width, y1+height/2].join(",");
			objekat.push(r.path(path));
			var strelica1 = ["M", x1+width/2 - 7,  y1-10, "L", x1+width/2, y1].join(",");
			objekat.push(r.path(strelica1));
			var strelica2 = ["M", x1+width/2 + 7,  y1-10, "L", x1+width/2, y1].join(",");
			objekat.push(r.path(strelica2));

			var centar = objekat[0].getPointAtLength(objekat[0].getTotalLength(objekat[0])/2);
			objekat.push(r.text(centar.x, centar.y, tekst).attr({fill: "#fff", "font-size": 12}));
			var textBBox = objekat[3].getBBox();
			objekat.push(r.rect(textBBox.x, textBBox.y, textBBox.width, textBBox.height, 5).attr({fill: "#000"}));
			objekat[3].toFront();
	}

	var update = function(dx, dy) {
		obj1 = this.pocetno_stanje.objekat[0];
		obj2 = this.zavrsno_stanje.objekat[0];
		if (obj1.id != obj2.id)	{
			var bb1 = obj1.getBBox(),
			bb2 = obj2.getBBox(),
			p = [{x: bb1.x + bb1.width / 2, y: bb1.y - 1},
			{x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},
			{x: bb1.x - 1, y: bb1.y + bb1.height / 2},
			{x: bb1.x + bb1.width + 1, y: bb1.y + bb1.height / 2},
			{x: bb2.x + bb2.width / 2, y: bb2.y - 1},
			{x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},
			{x: bb2.x - 1, y: bb2.y + bb2.height / 2},
			{x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}],
			d = {}, dis = [];
			for (var i = 0; i < 4; i++) {
				for (var j = 4; j < 8; j++) {
					var dx = Math.abs(p[i].x - p[j].x),
						dy = Math.abs(p[i].y - p[j].y);
					if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
						dis.push(dx + dy);
						d[dis[dis.length - 1]] = [i, j];
					}
				}
			}
			if (dis.length == 0) {
				var res = [0, 4];
			} else {
				res = d[Math.min.apply(Math, dis)];
			}
			var x1 = p[res[0]].x,
				y1 = p[res[0]].y,
				x4 = p[res[1]].x,
				y4 = p[res[1]].y;
			dx = Math.max(Math.abs(x1 - x4) / 2, 10);
			dy = Math.max(Math.abs(y1 - y4) / 2, 10);
			var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
				y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
				x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
				y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);
			if (res[1] == 6) {
				// strelica udesno
				var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", Number(x2)+10, Number(y2), Number(x3)-40, Number(y3), Number(x4.toFixed(3)), Number(y4.toFixed(3))].join(",");
				this.objekat[0].attr("path", path);
				var strelica1 = ["M", Number(x4.toFixed(0))-10,  Number(y4.toFixed(0))-7, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				this.objekat[1].attr("path", strelica1);
				var strelica2 = ["M", Number(x4.toFixed(0))-10, Number(y4.toFixed(0))+7, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				this.objekat[2].attr("path", strelica2);

			} else if (res[1] == 7) {
				// strelica ulijevo
				var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", Number(x2)+10, Number(y2), Number(x3)+40, Number(y3), Number(x4.toFixed(3)), Number(y4.toFixed(3))].join(",");
				this.objekat[0].attr("path", path);
				var strelica1 = ["M", Number(x4.toFixed(0))+10,  Number(y4.toFixed(0))-7, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				this.objekat[1].attr("path", strelica1);
				var strelica2 = ["M", Number(x4.toFixed(0))+10, Number(y4.toFixed(0))+7, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				this.objekat[2].attr("path", strelica2);
			} else if (res[1] == 4) {
				// strelica nadole
				var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", Number(x2)+10, Number(y2), Number(x3), Number(y3)-40, Number(x4.toFixed(3)), Number(y4.toFixed(3))].join(",");
				this.objekat[0].attr("path", path);
				var strelica1 = ["M", Number(x4.toFixed(0))-7,  Number(y4.toFixed(0))-10, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				this.objekat[1].attr("path", strelica1);
				var strelica2 = ["M", Number(x4.toFixed(0))+7, Number(y4.toFixed(0))-10, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				this.objekat[2].attr("path", strelica2);
			} else if (res[1] == 5) {
				// strelica nagore
				var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", Number(x2)+10, Number(y2), Number(x3), Number(y3)+40, Number(x4.toFixed(3)), Number(y4.toFixed(3))].join(",");
				this.objekat[0].attr("path", path);
				var strelica1 = ["M", Number(x4.toFixed(0))-7,  Number(y4.toFixed(0))+10, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				this.objekat[1].attr("path", strelica1);
				var strelica2 = ["M", Number(x4.toFixed(0))+7, Number(y4.toFixed(0))+10, "L", x4.toFixed(0), y4.toFixed(0)].join(",");
				this.objekat[2].attr("path", strelica2);
			}	
			
			var centar = this.objekat[0].getPointAtLength(this.objekat[0].getTotalLength(this.objekat[0])/2);
			this.objekat[3].attr({"x":centar.x, "y":centar.y, "text" : this.tekst});
			var textBBox = objekat[3].getBBox();
			objekat[4].attr({"x":textBBox.x, "y": textBBox.y, "width": textBBox.width, "height": textBBox.height});
		} else {
			var bbox = this.pocetno_stanje.objekat[0].getBBox();
			var x1 = bbox.x;
			var y1 = bbox.y;
			var width = bbox.width;
			var height = bbox.height;
			var path = ["M", x1+width/2, y1, "C", x1+width/2, y1-100, x1+width+100, y1+height/2, x1+width, y1+height/2].join(",");
			this.objekat[0].attr("path", path);
			var strelica1 = ["M", x1+width/2 - 7,  y1-10, "L", x1+width/2, y1].join(",");
			this.objekat[1].attr("path", strelica1);
			var strelica2 = ["M", x1+width/2 + 7,  y1-10, "L", x1+width/2, y1].join(",");
			this.objekat[2].attr("path", strelica2);

			var centar = objekat[0].getPointAtLength(objekat[0].getTotalLength(objekat[0])/2);
			this.objekat[3].attr({"x":centar.x, "y":centar.y, "text" : this.tekst});
			var textBBox = objekat[3].getBBox();
			objekat[4].attr({"x":textBBox.x, "y": textBBox.y, "width": textBBox.width, "height": textBBox.height});
			objekat[3].toFront();
		}
	};


	veza = {
		id: id,
		pocetno_stanje: pocetno_stanje,
		zavrsno_stanje: zavrsno_stanje,
		pravila: [pravilo],
		tekst: tekst,
		objekat: objekat,
		update: update
	}

	return veza;
}
