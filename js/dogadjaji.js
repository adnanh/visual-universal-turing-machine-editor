function inicijaliziraj_dogadjaje() {
	eve.on("stanje.deselektuj", stanje_deselektuj);
	eve.on("stanje.selektuj", stanje_selektuj);
	eve.on("stanje.mouseover", stanje_hover);
	eve.on("stanje.mouseout", stanje_hout);
	eve.on("stanje.tofront", stanje_tofront);
	eve.on("stanje.obrisi", stanje_obrisi);
	eve.on("stanje.oznaci.pocetno", stanje_oznaci_pocetno);
	eve.on("stanje.oznaci.terminalno", stanje_oznaci_terminalno);
	eve.on("objekat.selektovan", azuriraj_selektovani_objekat);

	eve.on("veze.azuriraj", veze_azuriraj);
	eve.on("veze.azuriraj.pravila", veze_azuriraj_pravila);
	eve.on("veze.obrisi", veze_obrisi);

	eve.on("gui.veze.obrisana", gui_veze_obrisana);
/*	eve.on("gui.veze.dodana", gui_azuriraj_veze);*/
	eve.on("gui.stanje.obrisano", gui_azuriraj_stanja);
}

function gui_veze_obrisana() {
	var pravila = document.getElementById("pravila");
	pravila.options.length = 0;
	for(var i=0; i < veze.length; i++) {
		if (veze[i].pocetno_stanje.id == selektovani_objekat.id) {	
			for (j = 0; j < veze[i].pravila.length; j++) {
				pravila.options.add(new Option(veze[i].pravila[j].procitano + " > " + veze[i].pravila[j].pisi + ", " + veze[i].pravila[j].pomjeri, veze[i].pravila[j].procitano));
			}
		}
	}
}

function gui_azuriraj_stanja() {
	eve("objekat.selektovan", "objekat", null);
}

function stanje_oznaci_terminalno(roditelj) {
	if (roditelj.tip == "terminalno_stanje") {
		// pretvori ga u obicno stanje
		roditelj.tip = "obicno_stanje";
		var x = roditelj.objekat[0].attr("cx"), y = roditelj.objekat[0].attr("cy");
		
		roditelj.objekat.undrag();

		for (i = 0; i < roditelj.objekat.length; i++) {
			roditelj.objekat[i].remove();
		}

		var novi = r.set();

		var krug = r.circle(x, y, 30).attr({"stroke" : "#ff0000", "fill": "#fff", "fill-opacity" : 1, "stroke-width": 2, "stroke-linecap": "round"});
		krug.data("roditelj", roditelj.id);
		krug.click(stanje_mouseclick);
		krug.dblclick(stanje_mousedblclick);
		krug.mouseover(stanje_mouseover);
		krug.mouseout(stanje_mouseout);
		novi.push(krug);
	
		var poc = false;
		if (pocetno != null) {
			if (pocetno.id == roditelj.id) {
				poc = true;
			}
		}

		var tekst_stanja = r.text(x, y, roditelj.tekst).attr({fill: "#ff0000", "font-size": ( poc == true ? 32 : 14), "font-weight" : "bold"});
		tekst_stanja.data("roditelj", roditelj.id);
		tekst_stanja.click(stanje_mouseclick);
		tekst_stanja.dblclick(stanje_mousedblclick);
		tekst_stanja.mouseover(stanje_mouseover);
		tekst_stanja.mouseout(stanje_mouseout);
		novi.push(tekst_stanja);
		
		novi.drag(stanje_move, stanje_dragover);

		roditelj.objekat = novi;
	} else {
		// pretvori ga u terminalno stanje
		roditelj.tip = "terminalno_stanje";
		var x = roditelj.objekat[0].attr("cx"), y = roditelj.objekat[0].attr("cy");
		
		roditelj.objekat.undrag();

		for (i = 0; i < roditelj.objekat.length; i++) {
			roditelj.objekat[i].remove();
		}
		var novi = r.set();

		var krug = r.circle(x, y, 30).attr({"stroke" : "#ff0000", "fill": "#fff", "fill-opacity" : 1, "stroke-width": 2, "stroke-linecap": "round"});
		krug.data("roditelj", roditelj.id);
		krug.click(stanje_mouseclick);
		krug.dblclick(stanje_mousedblclick);
		krug.mouseover(stanje_mouseover);
		krug.mouseout(stanje_mouseout);
		novi.push(krug);

		krug = r.circle(x, y, 25).attr({"stroke" : "#ff0000", "fill": "#fff", "fill-opacity" : 1, "stroke-width": 2, "stroke-linecap": "round"});
		krug.data("roditelj", roditelj.id);
		krug.click(stanje_mouseclick);
		krug.dblclick(stanje_mousedblclick);
		krug.mouseover(stanje_mouseover);
		krug.mouseout(stanje_mouseout);
		novi.push(krug);

		var poc = false;
		if (pocetno != null) {
			if (pocetno.id == roditelj.id) {
				poc = true;
			}
		}

		var tekst_stanja = r.text(x, y, roditelj.tekst).attr({fill: "#ff0000", "font-size": ( poc == true ? 32 : 14), "font-weight" : "bold"});
		tekst_stanja.data("roditelj", roditelj.id);
		tekst_stanja.click(stanje_mouseclick);
		tekst_stanja.dblclick(stanje_mousedblclick);
		tekst_stanja.mouseover(stanje_mouseover);
		tekst_stanja.mouseout(stanje_mouseout);
		novi.push(tekst_stanja);

		novi.drag(stanje_move, stanje_dragover);

		roditelj.objekat = novi;
	}

	var update = function(dx, dy) {
		if (roditelj.tip == "terminalno_stanje") {
			for (i = 0; i < roditelj.objekat.length; i++) {
				if (roditelj.objekat[i].type == "circle") {
					var x = (roditelj.objekat[i].attr("cx")) + dx;
					var y = (roditelj.objekat[i].attr("cy")) + dy;
					roditelj.objekat[i].attr({"cx":x, "cy":y});
				} else {
					var x = (roditelj.objekat[i].attr("x")) + dx;
					var y = (roditelj.objekat[i].attr("y")) + dy;
					roditelj.objekat[i].attr({"x": x, "y": y});
				}
			}
		} else {
			for (i = 0; i < roditelj.objekat.length; i++) {
				if (roditelj.objekat[i].type == "circle") {
					var x = (roditelj.objekat[i].attr("cx")) + dx;
					var y = (roditelj.objekat[i].attr("cy")) + dy;
					roditelj.objekat[i].attr({"cx":x, "cy":y});
				} else {
					var x = (roditelj.objekat[i].attr("x")) + dx;
					var y = (roditelj.objekat[i].attr("y")) + dy;
					roditelj.objekat[i].attr({"x": x, "y": y});
				}
			}
		}
	};

	roditelj.update = update;

}

function stanje_oznaci_pocetno(roditelj) {
	if (pocetno != null) {
		for (i = 0; i < pocetno.objekat.length; i++) {
			if (pocetno.objekat[i].type == "text") {
				pocetno.objekat[i].animate({"font-size": 15, "font-weight" : "normal"}, 300);
				break;
			}
		}
	}

	if (roditelj == null) {
		pocetno = null;
		return null;
	}

	for (i = 0; i < roditelj.objekat.length; i++) {
		if (roditelj.objekat[i].type == "text") {
			roditelj.objekat[i].animate({"font-size": 32, "font-weight" : "bold"}, 300);
			break;
		}
	}
	pocetno = roditelj;
}

function stanje_obrisi(roditelj) {
	selektovani_objekat = null;
	if (pocetno != null) {
		if (pocetno.id == roditelj.id) {
			pocetno = null;
		}
	}
	var id = roditelj.id
	var ima = true;
	
	while (ima)	{
		ima = false;
		for (i = 0; i < veze.length; i++) {
			if (veze[i].pocetno_stanje.id == id || veze[i].zavrsno_stanje.id == id) {
				eve("veze.obrisi", "veze", veze[i]);
				ima = true;
				break;
			}
		}
	}

	var objekti = roditelj.objekat;
	for (i = 0; i < objekti.length; i++) {
		objekti[i].remove();
	}
	for (i = 0; i < stanja.length; i++) {
		if (stanja[i].id == roditelj.id) {
			stanja.splice(i, 1);
			eve("gui.stanje.obrisano", "stanje");
			return null;
		}
	}
}

function stanje_tofront(roditelj) {
	roditelj.objekat[0].toFront();
	if (roditelj.tip == "terminalno_stanje")	{
		roditelj.objekat[1].toFront();
		roditelj.objekat[2].toFront();
	} else {
		roditelj.objekat[1].toFront();
	}

	for (i = 0; i < veze.length; i++) {
		if (veze[i].pocetno_stanje.id == roditelj.id) {
			veze[i].objekat[0].toFront();
			veze[i].objekat[1].toFront();
			veze[i].objekat[2].toFront();
			veze[i].objekat[4].toFront();
			veze[i].objekat[3].toFront();
		}
	}
}

function stanje_hover(roditelj, boja) {
	if (selektovani_objekat != roditelj) {
		eve("stanje.tofront", "stanje", roditelj);
		eve("stanje.selektuj", "stanje", roditelj, boja);
	}
}

function stanje_hout(roditelj) {
	if (selektovani_objekat != roditelj) {
		eve("stanje.deselektuj", "stanje", roditelj);
	}
}

function stanje_selektuj(roditelj, boja) {
	var trajanje_animacije = 300;

	roditelj.objekat[0].animate({"stroke" : boja}, trajanje_animacije);		
	roditelj.objekat[0].node.style.cursor = "move";
	if (roditelj.tip == "terminalno_stanje")	{
		roditelj.objekat[1].animate({"stroke" : boja}, trajanje_animacije);
		roditelj.objekat[1].node.style.cursor = "move";
		roditelj.objekat[2].animate({"fill" : boja}, trajanje_animacije);		
		roditelj.objekat[2].node.style.cursor = "move";
	} else {
		roditelj.objekat[1].animate({"fill" : boja}, trajanje_animacije);
		roditelj.objekat[1].node.style.cursor = "move";
	}

	for (i = 0; i < veze.length; i++) {
		if (veze[i].pocetno_stanje.id == roditelj.id) {
			veze[i].objekat[0].animate({"stroke": boja, "stroke-width": 2}, trajanje_animacije);
			veze[i].objekat[1].animate({"stroke": boja, "stroke-width": 2}, trajanje_animacije);
			veze[i].objekat[2].animate({"stroke": boja, "stroke-width": 2}, trajanje_animacije);
			veze[i].objekat[4].animate({"stroke": boja, "fill": boja}, trajanje_animacije);
		}
	}
}

function stanje_deselektuj(roditelj) {
	var trajanje_animacije = 300;

	roditelj.objekat[0].animate({"stroke" : "#000000"}, trajanje_animacije);
	roditelj.objekat[0].node.style.cursor = "default";
	if (roditelj.tip == "terminalno_stanje")	{
		roditelj.objekat[1].animate({"stroke" : "#000000"}, trajanje_animacije);	
		roditelj.objekat[1].node.style.cursor = "default";
		roditelj.objekat[2].animate({"fill" : "#000000"}, trajanje_animacije);	
		roditelj.objekat[2].node.style.cursor = "default";
	} else {
		roditelj.objekat[1].animate({"fill" : "#000000"}, trajanje_animacije);
		roditelj.objekat[1].node.style.cursor = "default";
	}

	for (i = 0; i < veze.length; i++) {
		if (veze[i].pocetno_stanje.id == roditelj.id) {
			veze[i].objekat[0].animate({"stroke": "#000000", "stroke-width":1}, trajanje_animacije);
			veze[i].objekat[1].animate({"stroke": "#000000", "stroke-width":1}, trajanje_animacije);
			veze[i].objekat[2].animate({"stroke": "#000000", "stroke-width":1}, trajanje_animacije);
			veze[i].objekat[4].animate({"stroke": "#000000", "fill": "#000000"}, trajanje_animacije);
		}
	}
}

function azuriraj_selektovani_objekat(roditelj) {
	var el2 = document.getElementById("stanje-toolbox");
	if (roditelj == null) {
		el2.style.display="none";
	} else {
		var oznaceno_stanje = document.getElementById("oznaceno_stanje");
		var pocetno_cbox = document.getElementById("pocetno");
		var terminalno_cbox = document.getElementById("terminalno");

		el2.style.display="block";
		oznaceno_stanje.innerHTML = roditelj.tekst;
		if (pocetno != null) {
			if (pocetno.id == roditelj.id) {
				pocetno_cbox.checked = true;
			} else {
				pocetno_cbox.checked = false;
			}
		} else {
			pocetno_cbox.checked = false;
		}	
		if (roditelj.tip == "terminalno_stanje")	{
			terminalno_cbox.checked = true;
		} else {
			terminalno_cbox.checked = false;
		}

		var pravila = document.getElementById("pravila");
		pravila.options.length = 0;
		for(var i=0; i < veze.length; i++) {
			if (veze[i].pocetno_stanje.id == roditelj.id) {	
				for (j = 0; j < veze[i].pravila.length; j++) {
					pravila.options.add(new Option(veze[i].pravila[j].procitano + " > " + veze[i].pravila[j].pisi + ", " + veze[i].pravila[j].pomjeri, veze[i].pravila[j].procitano));
				}
			}
		}
	}
}


function veze_azuriraj(roditelj) {
	for (i = 0; i < veze.length; i++) {
		veze[i].update();
	}
}

function veze_azuriraj_pravila(veza) {
	var pravila = veza.pravila;
	var tekst = "";
	for (j = 0; j < pravila.length; j++) {
		tekst += "\n" + pravila[j].procitano + " > " + pravila[j].pisi + ", " + pravila[j].pomjeri;
	}
	veza.tekst = tekst;
	var trajanje_animacije = 300;
	var boja = "#ff0000";
	veza.objekat[0].animate({"stroke": boja, "stroke-width": 2}, trajanje_animacije);
	veza.objekat[1].animate({"stroke": boja, "stroke-width": 2}, trajanje_animacije);
	veza.objekat[2].animate({"stroke": boja, "stroke-width": 2}, trajanje_animacije);
	veza.objekat[4].animate({"stroke": boja, "fill": boja}, trajanje_animacije);
	veza.update();

	if (selektovani_objekat != null) {
		var pravila = document.getElementById("pravila");
		pravila.options.length = 0;
		for(var i=0; i < veze.length; i++) {
			if (veze[i].pocetno_stanje.id == selektovani_objekat.id) {	
				for (j = 0; j < veze[i].pravila.length; j++) {
					pravila.options.add(new Option(veze[i].pravila[j].procitano + " > " + veze[i].pravila[j].pisi + ", " + veze[i].pravila[j].pomjeri, veze[i].pravila[j].procitano));
				}
			}
		}
	}
}

function veze_dodaj(pocetak, kraj, pravilo) {
	for (i = 0; i < veze.length; i++) {
		if (veze[i].pocetno_stanje.id == pocetak.id && veze[i].zavrsno_stanje.id == kraj.id) {
			veze_dodaj_pravilo(veze[i], pravilo);
			eve("veze.azuriraj.pravila", "veze", veze[i]);
			return null;
		}
	}

	for (i = 0; i < veze.length; i++) {
		if (veze[i].pocetno_stanje.id == pocetak.id) {
			for (j = 0; j < veze[i].pravila.length; j++) {
				if (veze[i].pravila[j].procitano == pravilo.procitano) {
					return null;
				}
			}
		}
	}

	veze.push(r.veza(0, pravilo, pocetak, kraj, r));
	eve("veze.azuriraj.pravila", "veze", veze[veze.length-1]);
}

function veze_dodaj_pravilo(veza, pravilo) {
	var pravila = veza.pravila;
	for (j = 0; j < pravila.length; j++) {
		if (pravila[j].procitano == pravilo.procitano) {
			return null;
		}
	}
	veza.pravila.push(pravilo);
}

function veze_obrisi_pravilo(veza, pravilo) {
	var pravila = veza.pravila;
	for (j = 0; j < pravila.length; j++) {
		if (pravila[j].procitano == pravilo.procitano) {
			pravila.splice(j, 1);
			if (pravila.length == 0) {
				eve("veze.obrisi", "veze", veza);
				return null;
			}
			eve("veze.azuriraj.pravila", "veze", veza);
			return null;
		}
	}
}

function veze_obrisi(veza) {
	var objekti = veza.objekat;
	for (i = 0; i < objekti.length; i++) {
		objekti[i].remove();
	}

	for (i = 0; i < veze.length; i++) {
		if (veze[i].pocetno_stanje.id == veza.pocetno_stanje.id && veze[i].zavrsno_stanje.id == veza.zavrsno_stanje.id) {
			veze.splice(i, 1);
			eve("gui.veze.obrisana", "veze", veza.pocetno_stanje);
			return null;
		}
	}
}